<?php
/**
 * @file
 * Wrapper template for fields with emphasis formatter.
 *
 * Custom variables:
 * - $output: rendered field markup.
 */
?>
<div id="article-content">
  <?php print $output; ?>
</div>
